Module support Export Product
# Installation
```bash
composer config repositories.pim-export-product vcs git@gitlab.com:dylanngo/pim_export_product.git
composer require pim/export-product
php bin/magento module:enable PIM_ExportProduct
```
## Changelog
* 1.0.0 Init
* 1.0.1 Update readme
* 1.0.2 Add system settings
